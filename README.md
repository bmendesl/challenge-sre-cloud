# Golang App

## Getting started

This project is for build a docker image with an golang application.

It was used:

- Docker multi-stage to reduce the size of the golang application
- Lint for the Dockerfile
- Build for docker image
- Shell script for testing the no-root user
- Push to docker hub

## Dependencies

- Docker 20.0.0 or above
- Account at docker hub - [Docker hub](https://hub.docker.com/)


## Executing locally

If you need to execute locally, follow:

- Step 1: clone this repo:

```
git clone https://gitlab.com/challenge-sre-cloud/golang-app.git
cd challenge-sre-cloud
```

- Step 2: build and run the image:

```
docker build -t <image name>:<tag> .
docker run -dti -p 8080:8080 <image name>:<tag>
```

- Step 3: test the application

```
curl http://127.0.0.1:8080
```


## Did you fork this project?

If you forked this project for your own use, please go to your project's Settings and remove the forking relationship, which won't be necessary unless you want to contribute back to the upstream project.

Do not forget to set the variables DOCKER_USERNAME DOCKER_PASSWORD at you project's Settings > CI/CD > Variables.

OBS.: the variable DOCKER_PASSWORD must be in base64.

## Support
If you need any help with this code send me an email: bruno@bruno.com.br

## Roadmap
To do:

- Test the application
- Push the image as develop and as homologation before push to production. This way can be deployed and tested properly in the development and homologation environments.

## Contributing
If you want to contribute, you can open an issue or if you prefer be more activelly you can fork this project and make a pull request to a new branch into the project.
