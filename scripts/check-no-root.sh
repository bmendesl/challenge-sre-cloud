#!/bin/sh

# This scripts checks if the container image has no-root user

container_name=${1}

command_whoami=$(docker exec -ti ${container_name} whoami | tr -d ' ')
date_now=$(date +"%d-%m-%Y %H-%M-%S")

if [[ "${command_whoami}" =~ .*"root".* ]]
then
    echo "$date_now - ERROR - Attention! Container has root user, please create a image that starts with non-root user!"
    exit 1
fi

echo "$date_now - INFO - Container started as non-root user - ${command_whoami}"
exit 0
